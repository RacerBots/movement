local pi = 3.14159265359
local rad2deg = 360 / (pi * 2)


function create_movement_instance()

	function angle(ax, ay, bx, by)
		local dx, dy = v2_delta(ax, ay, bx, by)
		local a = math.atan2(dy, dx)
		a = a * rad2deg
		if a < 0.0 then
			a = a + 360.0
		end
		a = a - 90.0
		if a < 0 then
			a = a + 360.0
		end
		return a
	end
	
	function toggle_function(_on, _off, reset_time)
		local self = {}
		self.on = _on
		self.off = _off
		self.is_toggled = false
		
		return {
			toggle = function()
				self.is_toggled = not self.is_toggled
				if self.is_toggled then
					self.on()
				else
					self.off()
				end
			end,
			
			turn_off = function()
				if self.is_toggled then
					self.is_toggled = false
					self.off()
				end
			end,
			
			turn_on = function()
				if not self.is_toggled then
					self.is_toggled = true
					self.on()
				end
			end,
			
			force_on = function()
				self.is_toggled = true
				self.on()
			end,
			
			force_off = function()
				self.is_toggled = false
				self.off()
			end
		}
	end

	local self = {}
	
	self.verbose_mode = true
	
	function verbose_print(v)
		if(self.verbose_mode) then
			print(v)
		end
	end
	
	self.frame_length = 1 / 60
	self.last_time = GetTime()
	
	self.set_map_delta_time = 5
	self.set_map_last_time = GetTime()
	
	self.turn_limit = 20
	self.move_forward_limit = 45
	
	self.last_pos = { x = 0, y = 0 }
	self.last_angle = 0
	
	self.turn_right = toggle_function(function()
		TurnRightStart(GetTime())
		verbose_print("Starts turning right")
	end, function()
		TurnRightStop(GetTime())
		verbose_print("Stops turning right")
	end)
	self.turn_left = toggle_function(function()
		TurnLeftStart(GetTime())
		verbose_print("Starts turning left")
	end, function()
		TurnLeftStop(GetTime())
		verbose_print("Stops turning left")
	end)
	self.move_forward = toggle_function(function()
		MoveForwardStart(GetTime())
		verbose_print("Starts moving forward")
	end, function()
		MoveForwardStop(GetTime())
		verbose_print("Stops moving forward")
	end)
	
	self.frame = CreateFrame("FRAME", "FooBarZebra")
	self.frame:SetPoint("CENTER", 0, 0)
	self.frame:SetHeight(10)
	self.frame:SetWidth(10)
	
	self.goal_pos = nil
	
	function set_continent_zoom()
		local map_id = GetCurrentMapContinent()
		SetMapZoom(map_id)
	end
	
	function update()
		if not (self.goal_pos == nil) then
			-- Turn
			set_continent_zoom()
			local pa = GetPlayerFacing() * rad2deg
			local px, py = GetPlayerMapPosition("player")
			local ga = angle(self.goal_pos.x, self.goal_pos.y, px, py)
			local da = ga - pa
			
			if self.last_pos.x == px and self.last_pos.y == py then
				self.move_forward.force_off()
			end
			self.last_pos.x = px
			self.last_pos.y = py
			
			--if self.last_angle == pa then
			--	self.turn_left.force_off()
			--	self.turn_right.force_off()
			--end
			--self.last_angle = pa
			
			local turn_limit = self.turn_limit
			if da > 0.0 + turn_limit and da < 180 then -- Turn left
				self.turn_right.turn_off()
				self.turn_left.turn_on()
			elseif da < 360.0 - turn_limit and da > 180 then -- Turn right
				self.turn_left.turn_off()
				self.turn_right.turn_on()
			else
				self.turn_right.turn_off()
				self.turn_left.turn_off()
			end
			
			if da < self.move_forward_limit or da > 360.0 - self.move_forward_limit then
				self.move_forward.turn_on()
			else
				self.move_forward.turn_off()
			end
		end
	end
	
	self.frame:SetScript("OnUpdate", function()
		local now = GetTime()
		local delta = now - self.last_time
		if delta > self.frame_length then
			self.last_time = now
			update()
		end
	end)
	self.frame:Show()
	
	return {
		rotation_radians = function()
			return GetPlayerFacing()
		end,
	
		rotation_deg = function()
			return GetPlayerFacing() * rad2deg
		end,
		
		continent_position = function(unit)
			set_continent_zoom()
			local x, y = GetPlayerMapPosition(unit)
			return x, y
		end,
		
		move_to_pos = function(x, y)
			self.goal_pos = { x = x, y = y }
		end
	}
end